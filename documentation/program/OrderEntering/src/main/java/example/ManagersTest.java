package example;

import java.util.HashMap;

public class ManagersTest implements Managers {
    private HashMap<String,Manager> managers;

    public ManagersTest(){
        managers = new HashMap<String, Manager>();
        Manager manager1 =  new Manager("Ivanov","Ivan","Ivanovich",78962);
        Manager manager2 = new Manager("Ivav","Iva","Ivanoh",7896);
        Manager manager3 = new Manager("Ivfdsav","Igtrhva","Ivanzxcoh",789656);
        managers.put("1",manager1);
        managers.put("2",manager2);
        managers.put("3",manager3);
    }

    public Manager getManager(String id) {

        return managers.get(id);
    }
}
