package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;


//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
@SpringBootApplication
public class AisApplication extends SpringBootServletInitializer
 {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AisApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(AisApplication.class, args);
    }
}
