package example;

import java.util.ArrayList;
import java.util.Date;


public class Order {
    private String id_order;
    private ArrayList<ProductinOrder> products;
    private String typeofreceipt;
    private String selfdischargepoint;
    private String placeofdekivery;
    private Client client;
    private Manager manager;
    private Boolean cashlesspayment;
    private String comment;
    private Status_order status;
    private Date dateofcompletion;
    private Date dateofissue;
    private Date dateofpayment;


    public Order(){}

    public Order(String id_order, String typeofreceipt, String selfdischargepoint, String placeofdekivery, Client client, Boolean cashlesspayment, String comment) {
        this.id_order = id_order;
        this.products = new ArrayList<ProductinOrder>();
        this.typeofreceipt = typeofreceipt;
        this.selfdischargepoint = selfdischargepoint;
        this.placeofdekivery = placeofdekivery;
        this.client = client;
        this.cashlesspayment = cashlesspayment;
        this.comment = comment;
        this.status = Status_order.created;
        this.manager = null;
        this.dateofcompletion=null;
        this.dateofissue=null;
        this.dateofpayment=null;
    }

    public Order(String id_order, String typeofreceipt, String selfdischargepoint, String placeofdekivery, Boolean cashlesspayment, String comment) {
        this.id_order = id_order;
        this.products = new ArrayList<ProductinOrder>();
        this.typeofreceipt = typeofreceipt;
        this.selfdischargepoint = selfdischargepoint;
        this.placeofdekivery = placeofdekivery;
        this.manager = null;
        this.cashlesspayment = cashlesspayment;
        this.comment = comment;
        this.status = Status_order.approved;
        this.client = null;
        this.dateofcompletion=null;
        this.dateofissue=null;
        this.dateofpayment=null;
    }

    public String getId_order() {
        return id_order;
    }

    public ArrayList<ProductinOrder> getProducts() {
        return products;
    }

    public String getTypeofreceipt() {
        return typeofreceipt;
    }

    public String getSelfdischargepoint() {
        return selfdischargepoint;
    }

    public String getPlaceofdekivery() {
        return placeofdekivery;
    }

    public Client getClient() {
        return client;
    }

    public Manager getManager() {
        return manager;
    }

    public Boolean getCashlesspayment() {
        return cashlesspayment;
    }

    public String getComment() {
        return comment;
    }

    public Status_order getStatus() {
        return status;
    }

    public Date getDateofcompletion() {
        return dateofcompletion;
    }

    public Date getDateofissue() {
        return dateofissue;
    }

    public Date getDateofpayment() {
        return dateofpayment;
    }

    public void setStatus(Status_order status) throws Exception {

        if (this.status==Status_order.created )
            if (status==Status_order.submitted_of_approval || status==Status_order.approved || status==Status_order.canceled)
            {
                this.status = status;
                return;
            }
            else
                throw new Exception();

        if (this.status==Status_order.submitted_of_approval)
            if (status==Status_order.approved || status==Status_order.canceled)
            {
                this.status = status;
                return;
            }
            else
                throw new Exception();

        if (this.status==Status_order.approved)
            if (status==Status_order.paid || status == Status_order.delivered)
            {
                this.status = status;
                return;
            }
            else
                throw new Exception();

        throw new Exception();

    }

    public void setProducts(ArrayList<ProductinOrder> products) {
        this.products = products;
    }

    public void setClient(Client client) throws Exception {
        if (this.client==null)
            this.client = client;
        else
            throw new Exception();
    }

    public void setManager(Manager manager) throws Exception {
        if (this.manager==null)
            this.manager = manager;
        else {
            throw new Exception();
        }
    }

    public Double getOrderCost(){
        Double cost=0.00;
        Integer i=0;
        if (products.size()==0)
            cost=0.00;
        while (i<products.size())
        {
            cost= cost + products.get(i).getPrice();
            i++;
        }
        return cost;
    }

    public ProductinOrder findProductInOrder(ProductinOrder productinOrder){
        Integer i=0;
        while (i<products.size()) {
            if (productinOrder.getProduct().getIdproduct() == products.get(i).getProduct().getIdproduct())
                return products.get(i);
            i++;
        }
        return null;
    }

    public Double getCostofProductinOrder(ProductinOrder product){
        Double cost=0.00;
        Integer i=0;
        while (i<products.size()) {
            if (product.getProduct().getIdproduct() == products.get(i).getProduct().getIdproduct())
                return products.get(i).getPrice();
            i++;
        }
        return cost;
    }

    public void AddProductinOrder (ProductinOrder productinOrder) throws Exception {
         Boolean findproducrt = false;
         Integer i=0;
             while (i < products.size()) {
                 if (products.get(i).getProduct().getIdproduct() == productinOrder.getProduct().getIdproduct()) {
                     findproducrt = true;
                     break;
                 }
                 i++;
             }
             if (findproducrt) {
                 products.get(i).Addproduct(productinOrder.getAmount());
             } else {
                 products.add(productinOrder);
             }

    }

    public void DeleteProductinOrder (Product product, Integer count) throws Exception {
        Boolean findproduct = false;
        Integer i=0;
        while (i<products.size())
        {
            if(products.get(i).getProduct().getIdproduct()==product.getIdproduct())
            {
                findproduct=true;
                break;
            }
            i++;
        }
        if (findproduct)
        {
            products.get(i).setAmount(count);

        }
        else
        {
            throw new Exception();
        }

    }
    public void ApproveAnOrder(Manager manager) throws Exception {
        if (this.getOrderCost() > 1000) {
            if (this.status != Status_order.canceled) {
                this.status = Status_order.approved;
                this.manager = manager;
            } else {
                throw new Exception("bad");
            }
        }
        else
            this.status=Status_order.approved;
    }
}
