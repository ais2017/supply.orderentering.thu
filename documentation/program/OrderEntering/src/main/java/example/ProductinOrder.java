package example;

public class ProductinOrder {
    private Product product;
    private Integer amount;


    public ProductinOrder(Product product, Integer amount) throws Exception {
        if (product.getAmount()< amount || amount <0)
            throw new Exception();
        else
            this.product =product;
            product.setAmount(product.getAmount()-amount);
            this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public Integer getAmount() {
        return amount;
    }
    public Double getPrice(){
        return amount*product.getPrice();
    }


    public void Addproduct(Integer count){

        this.amount=this.amount+count;
    }

    public void setAmount(Integer amount) throws Exception {
        if (amount <0)
            throw new Exception();
        else
            if (this.amount-amount<0)
                throw new Exception();
            else {
                this.amount = this.amount-amount;
                this.product.setAmount(this.product.getAmount() + amount);
            }
    }
}
