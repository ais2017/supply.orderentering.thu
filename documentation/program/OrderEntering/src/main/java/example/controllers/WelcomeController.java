package example.controllers;


import example.Manager;
import example.Order;
import example.Product;
import example.Scenario;
import example.repository.DataAccessRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class WelcomeController {

    private final Scenario scenario = new Scenario(new DataAccessRepository());

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        String message = "welcome";
        model.addAttribute("message", message);
        return "index";
    }

    @RequestMapping(value = {"/show/orders"}, method = RequestMethod.GET)
    public String showOrders(Model model) {
        Manager manager = new Manager();
        try {
            ArrayList<Order> list = scenario.ShowAllOrder(manager);
            model.addAttribute("message",list);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "orders";
    }

    @RequestMapping(value = {"/show/my/orders"}, method = RequestMethod.GET)
    public String showMyOrders(Model model,
                               @RequestParam(value = "client_id") String id) {
        try {
            ArrayList<Order> list = scenario.ShowMyOrder(id);
            model.addAttribute("message",list);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "orders";
    }

    @RequestMapping(value = {"/show/products"}, method = RequestMethod.GET)
    public String showProducts(Model model) {
        ArrayList<Product> list = scenario.getProducts();
        model.addAttribute("message",list);
        return "products";
    }

    @RequestMapping(value = {"/form/order"}, method = RequestMethod.GET)
    public String createOrderForm(Model model) {
        return "order_form";
    }


    @RequestMapping(value = {"/create/order/client"}, method = RequestMethod.POST)
    public String createOrderClient(Model model, @RequestBody Order order) {
//        scenario.CreateOrderByClient();
        return "orders";
    }

    @RequestMapping(value = {"/create/order/manager"}, method = RequestMethod.POST)
    public String createOrderManager(Model model, @RequestBody Order order) {
//        scenario.CreateOrderByManager();
        return "orders";
    }

}