package example;

import java.util.ArrayList;

public class AbstarctReleaseTestRelease implements AbstractRelease {

    private ArrayList<Order> orders;
    private ArrayList<Product> products;
    public AbstarctReleaseTestRelease (){
        this.orders=new ArrayList<Order>();
        this.products=new ArrayList<Product>();
    }

    public void CreateProductList(){
        Product product1 = new Product("car","14x63",null,500.10,"cars",50);
        Product product2 = new Product("car","14xx1",null,5000.10,"cars S class",10);
        Product product3 = new Product("moto","4x63","kawasaki bike",2500.10,"moto",12);
        Product product4 = new Product("PC","1460x","HP model",147.00,"PC computers",7);
        Product product5 = new Product("Monitor","14x6"," DEll monitor",14.10,"Monitor",4);
        Product product6 = new Product("Apple 5s","14x3","Apple Phone",50.10,"Smarthone",17);
        Product product7 = new Product("Apple 6","1463","Apple Phone",150.10,"Smarthone",36);
        Product product8 = new Product("Apple 6s","1x63","Apple Phone",250.10,"Smarthone",14);
        Product product9 = new Product("Apple 7","146","Apple Phone",300.10,"Smarthone",12);
        Product product10 = new Product("Apple 8","x63","Apple Phone",350.10,"Smarthone",9);
        Product product11 = new Product("Apple x","14x","Apple Phone",500.10,"Smarthone",3);
        Product product12 = new Product("car","xx8931","BMW",256.59,"cars",8);
        Product product13 = new Product("car","xx893","Lada",50.10,"cars",9);
        Product product14 = new Product("car","xx89","Toyota",190.10,"cars",7);
        Product product15 = new Product("car","xx89319","Nissan",168.78,"cars",3);
        Product product16 = new Product("car","xx8978","Mersedez",300.10,"cars",4);
        Product product17 = new Product("car","xx893132","Kia",120.12,"cars",0);
        Product product18 = new Product("car","xx7862","Lexus",280.14,"cars",0);
        Product product19 = new Product("car","xx1324","Porshe",358.15,"cars",7);
        Product product20 = new Product("car","xx123456","Chevrolet",224.19,"cars",1);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        products.add(product4);
        products.add(product5);
        products.add(product6);
        products.add(product7);
        products.add(product8);
        products.add(product9);
        products.add(product10);
        products.add(product11);
        products.add(product12);
        products.add(product13);
        products.add(product14);
        products.add(product15);
        products.add(product16);
        products.add(product17);
        products.add(product18);
        products.add(product19);
        products.add(product20);
    }

    public ArrayList<Product> ShowProductList() {
        return products;
    }

    public void ForwardingOrder(Order order) {
        boolean result =false;
        int i = 0;
        while (i<this.orders.size())
        {
            if (order.getId_order()==this.orders.get(i).getId_order())
            {
                result = true;
                break;
            }
            i++;
        }
        if (!result)
        this.orders.add(order);
    }

    public ArrayList<Order> ShowAllOrder(Manager manager){
            return orders;
    }

    public ArrayList<Order> ShowYourOrder(Client client) {
        ArrayList<Order> ClientOrders = new ArrayList<Order>();
        Integer i =0;
        while (i < orders.size()){
            if (orders.get(i).getClient()==client)
                ClientOrders.add(orders.get(i));
            i++;
        }
        return ClientOrders;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public Order findOrder(String idOrder)
    {
        boolean result = false;
        int i = 0;
        while (i<this.orders.size())
        {
            if (idOrder.equals(this.orders.get(i).getId_order()))
            {
                result = true;
                break;
            }
            i++;
        }
        if (result)
            return this.orders.get(i);
        else
            return null;
    }
    public void UpdateOrder(Order order) throws Exception {
        if (findOrder(order.getId_order())!=null) {
            (findOrder(order.getId_order())).setProducts(order.getProducts());
        }
        else
            throw new Exception();

    }
}
