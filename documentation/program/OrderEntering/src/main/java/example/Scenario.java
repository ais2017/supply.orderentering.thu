package example;


import java.util.ArrayList;


public class Scenario {
    private AbstractRelease abstarctReleaseTestRelease;
    private ClientsTest clients;
    private ManagersTest managers;

    // инициализация системы
    public Scenario (AbstractRelease ReleaseTestRelease){
        abstarctReleaseTestRelease = ReleaseTestRelease ;
        //abstarctReleaseTestRelease.CreateProductList();
        managers = new ManagersTest();
        clients = new ClientsTest();
    }

        //список вернется пустым для данного клиента
    public ArrayList<Order> ShowMyOrder(String id) {

        return abstarctReleaseTestRelease.ShowYourOrder(clients.getClient(id));
    }

    // выдача всех заказов может запросить только менеджер
    public ArrayList<Order> ShowAllOrder(Manager manager) throws Exception {
        return abstarctReleaseTestRelease.ShowAllOrder(manager);
    }

    public int CheckProduct(ArrayList<ProductfromClient> products)
    {
        ArrayList<Product> ProductinMagazine = getProducts();
        int i=0,count=0;
        while (i < products.size())
        {
            int j = 0;
            while (j < ProductinMagazine.size()) {
                if (products.get(i).getId_product().equals(ProductinMagazine.get(j).getIdproduct()) && products.get(i).getAmount() <= ProductinMagazine.get(j).getAmount()) {
                    count ++;
                }
                j++;
            }
            i++;
        }
        return count;
    }

    public AbstractRelease CreateOrderByManager(String id_client, ArrayList<ProductfromClient> products, String id_manager, String id_order, String typeofreceipt, String selfdischargepoint, String placeofdekivery, Boolean cashlesspayment, String comment) throws Exception {
        ArrayList<Product> ProductinMagazine = getProducts();
        ArrayList<ProductinOrder> productinOrders = new ArrayList<ProductinOrder>();
        int i = 0, count = 0;
       count = CheckProduct(products);
        if (count==products.size())
        {
            if (clients.getClient(id_client)!=null ) {
                Order order = new Order(id_order, typeofreceipt, selfdischargepoint, placeofdekivery, cashlesspayment, comment);
                order.setClient(clients.getClient(id_client));
                order.setManager(managers.getManager(id_manager));
                i=0;
                while (i < products.size())
                {
                    productinOrders.add(new ProductinOrder(findProductbyId(ProductinMagazine,products.get(i).getId_product()),Math.abs(products.get(i).getAmount())));
                    if (products.get(i).getAmount() > 0)
                        order.AddProductinOrder(productinOrders.get(i));
                    else
                        order.DeleteProductinOrder(findProductbyId(ProductinMagazine,products.get(i).getId_product()),(-1)*products.get(i).getAmount());
                    i++;
                }
                abstarctReleaseTestRelease.ForwardingOrder(order);
                return abstarctReleaseTestRelease;
            }
            else {
                return null;
            }
        }
        else
            return null;

    }

    public  ArrayList<Product> getProducts(){
        return abstarctReleaseTestRelease.getProducts();
    }


    public AbstractRelease CreateOrderByClient(ArrayList<ProductfromClient> products,String client, String id_order, String typeofreceipt, String selfdischargepoint, String placeofdekivery, Boolean cashlesspayment, String comment) throws Exception {
        ArrayList<ProductinOrder> productinOrders = new ArrayList<ProductinOrder>();
        ArrayList<Product> ProductinMagazine = getProducts();
        int i = 0, count = 0;
        if (clients.getClient(client)!=null)
        {
        count = CheckProduct(products);
        Order order = new Order(id_order, typeofreceipt, selfdischargepoint, placeofdekivery, clients.getClient(client), cashlesspayment, comment);

        if (count==products.size()) {
            i = 0;
            while (i < products.size()) {
                productinOrders.add(new ProductinOrder(findProductbyId(ProductinMagazine, products.get(i).getId_product()), Math.abs(products.get(i).getAmount())));//число другое
                if (products.get(i).getAmount() > 0)
                    order.AddProductinOrder(productinOrders.get(i));
                else
                    order.DeleteProductinOrder(findProductbyId(ProductinMagazine,products.get(i).getId_product()), (-1) * products.get(i).getAmount());
                i++;
            }
            if (order.getOrderCost()<1000)
            {
                order.setStatus(Status_order.approved);
                abstarctReleaseTestRelease.ForwardingOrder(order);
            }
            else
            {
                order.setStatus(Status_order.submitted_of_approval);
                abstarctReleaseTestRelease.ForwardingOrder(order);
            }
        }
        else
        {
            order.setStatus(Status_order.canceled);
            abstarctReleaseTestRelease.ForwardingOrder(order);
        }


        return abstarctReleaseTestRelease;
    }
    else
        {
            return null;
        }
    }


    private Product findProductbyId (ArrayList<Product> products, String id_product)
    {
        int i = 0;
        while (i < products.size())
        {
            if (products.get(i).getIdproduct().equals(id_product))
                return products.get(i);
            i++;
        }
        return null;
    }
}
