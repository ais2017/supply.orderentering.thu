package example;

public class ProductfromClient {
    private String id_product;
    private Integer amount;


    public ProductfromClient(String id, int amount)
    {
        this.id_product=id;
        this.amount=amount;
    }

    public String getId_product() {
        return id_product;
    }

    public Integer getAmount() {
        return amount;
    }
}
