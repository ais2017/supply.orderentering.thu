package example;

import java.util.HashMap;

public class ClientsTest implements Clients{

    private HashMap<String,Client> clients;

    public ClientsTest(){
        clients = new HashMap<String, Client>();
        Client client1 = new  Client("Ivanov", "Ivan", "Ivanovich", "Pobeda 13", 999999, "mail@mail.ru");
        Client client2 = new Client("Petrov", "Ivan", "Ivanovich","Mira 15", 666666, "pochta@mail.ru");
        Client client3 = new Client("Volkov", "Ivan", "Ivanovich","Kazan Mira 15", 22222, "poc@mail.ru");
        clients.put("1",client1);
        clients.put("2",client2);
        clients.put("3",client3);
    }

    public Client getClient(String id) {

        return clients.get(id);
    }
}
