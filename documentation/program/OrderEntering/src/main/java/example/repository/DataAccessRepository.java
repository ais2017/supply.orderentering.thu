package example.repository;


import example.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;


@Service
public class DataAccessRepository implements AbstractRelease{

    @Primary
    @Bean(name = "mainDataBase")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource mainDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "mainDBJdbc")
    public JdbcTemplate mainJdbcTemplate(@Qualifier("mainDataBase") DataSource mainDataBase) {
        return new JdbcTemplate(mainDataBase);
    }

    @Autowired
    private JdbcTemplate jtm;

    public void CreateProductList(){ // см. инициализацию БД
    }

    public ArrayList<Product> ShowProductList(){
        List<Product> list;
        String sql = "SELECT * FROM Ordering";
        list = jtm.query(sql, new BeanPropertyRowMapper(Product.class));
        return (ArrayList<Product>) list;
    }

    public void ForwardingOrder(Order order){
        String sql = "INSERT INTO Ordering(order_id, typeofreceipt, selfdischargepoint, placeofdekivery, clientId, managerId, cashlesspayment)"+
                 "VALUES ("+order.getId_order()+","+order.getTypeofreceipt()+","+order.getSelfdischargepoint()+","+order.getPlaceofdekivery()+","+order.getClient().hashCode()+","+order.getManager().hashCode()+","+order.getCashlesspayment()+")";
        jtm.execute(sql);
    }

    public ArrayList<Order> ShowAllOrder(Manager manager) throws Exception{
        List<Order> list;

        String sql = "SELECT * FROM Ordering";
        list = jtm.query(sql, new BeanPropertyRowMapper(Order.class));
        return (ArrayList<Order>) list;
    }

    public ArrayList<Order> ShowYourOrder(Client client){
        List<Order> list;

        String sql = "SELECT * FROM Ordering";
        list = jtm.query(sql, new BeanPropertyRowMapper(Order.class));
        List<Order> newList = new ArrayList<Order>();
        for (Order order: list){
            if (order.getClient().equals(client)) newList.add(order);
        }
        return (ArrayList<Order>) list;
    }

    public Order findOrder(String idOrder){
        Order order;
        String sql = "SELECT * FROM Ordering WHERE idOrder="+idOrder;
        order = (Order) jtm.queryForObject(sql, new BeanPropertyRowMapper(Order.class));
        return order;
    }

    public void UpdateOrder(Order order) throws Exception{
        String sql = "UPDATE Ordering"+
                "SET typeofreceipt="+order.getTypeofreceipt()+", selfdischargepoint="+order.getSelfdischargepoint()+",placeofdekivery="+order.getPlaceofdekivery()+",clientId="+order.getClient().hashCode()+",managerId="+order.getManager().hashCode()+",cashlesspayment="+order.getCashlesspayment()+
                " WHERE order_id="+order.getId_order();
        jtm.execute(sql);
    }

    public ArrayList<Product> getProducts(){
        List<Product> list;
        String sql = "SELECT * FROM Product";
        list = jtm.query(sql, new BeanPropertyRowMapper(Product.class));
        return (ArrayList<Product>) list;
    }

    public ArrayList<Order> getOrders(){
        List<Order> list;

        String sql = "SELECT * FROM Ordering";
        list = jtm.query(sql, new BeanPropertyRowMapper(Order.class));
        return (ArrayList<Order>) list;
    }
}
