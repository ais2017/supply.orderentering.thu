package example;

public class Client {
    private String surname;
    private String name;
    private String middle_name;
    private String address;
    private Integer phone;
    private String mail;

    public Client(String surname, String name, String middle_name, String address, Integer phone, String mail) {
        this.surname = surname;
        this.name = name;
        this.middle_name = middle_name;
        this.address = address;
        this.phone = phone;
        this.mail = mail;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public String getAddress() {
        return address;
    }

    public Integer getPhone() {
        return phone;
    }

    public String getMail() {
        return mail;
    }
}
