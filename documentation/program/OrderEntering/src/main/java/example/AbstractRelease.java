package example;

import java.util.ArrayList;

public interface AbstractRelease {


    void CreateProductList();

    ArrayList<Product> ShowProductList();


    void ForwardingOrder(Order order);

    ArrayList<Order> ShowAllOrder(Manager manager) throws Exception;

    ArrayList<Order> ShowYourOrder(Client client);

    Order findOrder(String idOrder);

    void UpdateOrder(Order order) throws Exception;

    ArrayList<Product> getProducts();

    ArrayList<Order> getOrders();


}
