package example;

public class Manager {
    private String surname;
    private String name;
    private String middle_name;
    private Integer phone;

    public Manager(){};
    public Manager (String Surname, String Name, String Middle_name, Integer Phone )
    {
        this.middle_name=Middle_name;
        this.name=Name;
        this.surname=Surname;
        this.phone=Phone;
    }

    public String getName()
    {
        return name;
    }

    public String getMiddle_name()
    {
        return middle_name;
    }

    public String getSurname() {
        return surname;
    }

    public Integer getPhone() {
        return phone;
    }
}
