package example;

public class Product {
    private String name;
    private String idproduct;
    private String description;
    private Double price;
    private String category;
    private Integer amount;

    public Product(String name, String idproduct, String description, Double price, String category, Integer amount) {
        this.name = name;
        this.idproduct = idproduct;
        this.description = description;
        this.price = price;
        this.category = category;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public String getIdproduct() {
        return idproduct;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public String getCategory() {
        return category;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
