


CREATE TABLE IF NOT EXISTS Manager (
  id             BIGSERIAL,
  surname      VARCHAR(50)  NOT NULL,
  name     VARCHAR(50)  NOT NULL,
  middle_name     VARCHAR(50)  NOT NULL,
  phone          BIGINT NOT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS Client (
  id             BIGSERIAL,
  surname      VARCHAR(50)  NOT NULL,
  name     VARCHAR(50)  NOT NULL,
  middle_name     VARCHAR(50)  NOT NULL,
  phone          BIGINT NOT NULL,
  address          VARCHAR (50) NOT NULL,
  mail          VARCHAR (50) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Product (
  idproduct             BIGSERIAL,
  name      VARCHAR(50)  NOT NULL,
  description     TEXT  NOT NULL,
  price      REAL  NOT NULL,
  category          VARCHAR (50) NOT NULL,
  amount INTEGER NOT NULL,
  PRIMARY KEY (idproduct)
);


CREATE TYPE Status AS ENUM ('created', 'submitted_of_approval', 'approved', 'paid', 'delivered', 'canceled');

CREATE TABLE IF NOT EXISTS Ordering (
  id             BIGSERIAL,
  order_id VARCHAR(50)  NOT NULL UNIQUE,
  typeofreceipt      VARCHAR(50)  NOT NULL,
  selfdischargepoint    VARCHAR(100)  NOT NULL,
  placeofdekivery     VARCHAR(100)  NOT NULL,
  clientId BIGINT NOT NULL,
  managerId BIGINT NOT NULL,
  cashlesspayment BOOLEAN NOT NULL,
  comment TEXT DEFAULT NULL,
  status Status NOT NULL DEFAULT 'created',
  dateofcompletion TIMESTAMP DEFAULT NULL,
  dateofissue TIMESTAMP DEFAULT NULL,
  dateofpayment TIMESTAMP DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_Order_ClientId FOREIGN KEY (clientId) REFERENCES Client (id),
  CONSTRAINT FK_Order_ManagerId FOREIGN KEY (managerId) REFERENCES Manager (id)
);


CREATE TABLE IF NOT EXISTS ProductinOrder  (
  id            BIGSERIAL,
  productid  BIGINT,
  orderid BIGINT,
  PRIMARY KEY (id),
  CONSTRAINT FK_ProductinOrder_ProductId FOREIGN KEY (productid) REFERENCES Product (idproduct),
  CONSTRAINT FK_ProductinOrder_OrderId FOREIGN KEY (orderid) REFERENCES Ordering (id)
);