
DELETE FROM Manager;

INSERT INTO Manager(surname, name, middle_name, phone) VALUES
('Иванов', 'Иван', 'Иванович', 12345);

DELETE FROM Product;

INSERT INTO Product(name, description, price, category, amount) VALUES
('car1', 'desc', 223, 'category', 2);
INSERT INTO Product(name, description, price, category, amount) VALUES
('car2', 'desc', 223, 'category', 2);
INSERT INTO Product(name, description, price, category, amount) VALUES
('car3', 'desc', 223, 'category', 2);