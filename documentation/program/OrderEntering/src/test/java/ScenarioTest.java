import example.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ScenarioTest {

    Scenario scenario;
    Client client, client1;
    Manager manager;

    @Before
    public void CreateScenario(){
        AbstarctReleaseTestRelease abstarctReleaseTestRelease = new AbstarctReleaseTestRelease();
        scenario = new Scenario(abstarctReleaseTestRelease);
        client = new Client("Ivanov", "Ivan", "Ivanovich", "Pobeda 13", 999999, "mail@mail.ru");
        manager = new Manager("Ivanov","Ivan","Ivanovich",78962);
        client1 = new Client("Petrov", "Ivan", "Ivanovich","Mira 15", 666666, "pochta@mail.ru");

    }
    @Test
    public void showMyOrder() throws Exception {
        Product product1 = new Product("car","14x63",null,500.10,"cars",1);
        Product product5 = new Product("Monitor","14x6"," DEll monitor",14.10,"Monitor",3);
        Product product8 = new Product("Apple 6s","1x63","Apple Phone",250.10,"Smarthone",6);
        ProductfromClient prod1 = new ProductfromClient(product1.getIdproduct(),1), prod2 = new ProductfromClient(product5.getIdproduct(),3), prod3=new ProductfromClient(product8.getIdproduct(),6),prod4 = new ProductfromClient(product8.getIdproduct(),-2);
        ArrayList<ProductfromClient> pr1 = new ArrayList<ProductfromClient>(),pr2= new ArrayList<ProductfromClient>();
        pr1.add(prod1);
        pr1.add(prod2);
        pr2.add(prod3);
        pr2.add(prod4);
        String id_order="1",  typeofreceipt="gcgxrth", selfdischargepoint="Moscow Mira 15", placeofdekivery = null,   comment = "coment";
        boolean cashlesspayment=true;
        scenario.CreateOrderByManager("1",pr1,"2",id_order,typeofreceipt,selfdischargepoint,placeofdekivery,cashlesspayment,comment);
        id_order="2";
        typeofreceipt="pjutrv";
        selfdischargepoint="Moscow Lenina 3";
        AbstractRelease abstractRelease = scenario.CreateOrderByManager("2",pr2,"1",id_order,typeofreceipt,selfdischargepoint,placeofdekivery,cashlesspayment,comment);
        assertEquals(2, scenario.ShowAllOrder(manager).size());
        Integer produc1 = 49, produc2 =1,produc3 = 4;
        assertEquals(produc1,scenario.getProducts().get(0).getAmount());
        assertEquals(produc2,scenario.getProducts().get(4).getAmount());
        assertEquals(produc3,abstractRelease.getOrders().get(1).getProducts().get(0).getAmount());
        assertEquals(1,scenario.ShowMyOrder("1").size());
        assertEquals(2,scenario.ShowMyOrder("1").get(0).getProducts().size());
        assertEquals(1,scenario.ShowMyOrder("2").size());
        assertEquals(1,scenario.ShowMyOrder("2").get(0).getProducts().size());


    }


    @Test
    public void createOrderByManager() throws Exception {
        Product product1 = new Product("car","14x63",null,500.10,"cars",51);
        Product product5 = new Product("Monitor","1wwwww4x6"," DEll monitor",14.10,"Monitor",3);
        ArrayList<ProductfromClient> pr1 = new ArrayList<ProductfromClient>(),pr2= new ArrayList<ProductfromClient>() , pr3 = new ArrayList<ProductfromClient>();
        ProductfromClient prod1 = new ProductfromClient(product1.getIdproduct(),51), prod2 = new ProductfromClient(product5.getIdproduct(),3);
        pr1.add(prod1);
        pr2.add(prod2);
        String id_order="1",  typeofreceipt="gcgxrth", selfdischargepoint="Moscow Mira 15", placeofdekivery = null,   comment = "coment";
        boolean cashlesspayment=true;
        AbstractRelease abstarctReleaseTestRelease;
        abstarctReleaseTestRelease=scenario.CreateOrderByManager("1",pr1,"1",id_order,typeofreceipt,selfdischargepoint,placeofdekivery,cashlesspayment,comment);
        assertNull(abstarctReleaseTestRelease);
        id_order="2";
        typeofreceipt="gcgxrtgth";
        selfdischargepoint="Moscow Mira 15";
        placeofdekivery = null;
        comment = "coment";
        cashlesspayment=true;
        Product product3 = new Product("car","14x63",null,500.10,"cars",10);
        ProductfromClient prod3 = new ProductfromClient(product3.getIdproduct(),product3.getAmount());
        abstarctReleaseTestRelease=scenario.CreateOrderByManager("1",pr2,"1",id_order,typeofreceipt,selfdischargepoint,placeofdekivery,cashlesspayment,comment);
        assertNull(abstarctReleaseTestRelease);
        pr3.add(prod3);
        abstarctReleaseTestRelease=scenario.CreateOrderByManager("1",pr3,"1",id_order,typeofreceipt,selfdischargepoint,placeofdekivery,cashlesspayment,comment);
        Manager manager = new Manager("Ivanov","Ivan","Ivanovich",78962);
        assertEquals( manager.getName(),abstarctReleaseTestRelease.getOrders().get(0).getManager().getName());
        assertEquals(manager.getMiddle_name(),abstarctReleaseTestRelease.getOrders().get(0).getManager().getMiddle_name());
        assertEquals(manager.getSurname(),abstarctReleaseTestRelease.getOrders().get(0).getManager().getSurname());
        assertEquals(manager.getPhone(),abstarctReleaseTestRelease.getOrders().get(0).getManager().getPhone());

    }

    @Test
    public void getProducts() throws Exception {
        assertEquals(20,scenario.getProducts().size());
        Integer[] a = {50,10,12,7,4,17,36,14,12,9,3,8,9,7,3,4,0,0,7,1};
        int i = 0;
        while (i<scenario.getProducts().size())
        {
            assertEquals(a[i],scenario.getProducts().get(i).getAmount());
            i++;
        }

    }

    @Test
    public void createOrderByClient() throws Exception {
        Product product1 = new Product("car","14x63",null,500.10,"cars",1);
        Product product5 = new Product("Monitor","14x6"," DEll monitor",14.10,"Monitor",3);
        Product product8 = new Product("Apple 6s","1x63","Apple Phone",250.10,"Smarthone",6);
        ArrayList<ProductfromClient> prod1 = new ArrayList<ProductfromClient>(),prod2 = new ArrayList<ProductfromClient>();
        ProductfromClient pro1 = new ProductfromClient(product1.getIdproduct(),1), pro2 = new ProductfromClient(product5.getIdproduct(),3), pro3 = new ProductfromClient(product8.getIdproduct(),6);
        prod1.add(pro1);
        prod1.add(pro2);
        prod2.add(pro3);
        String id_order="1",  typeofreceipt="gcgxrth", selfdischargepoint="Moscow Mira 15", placeofdekivery = null,   comment = "coment";
        boolean cashlesspayment=true;
        AbstractRelease abstarctReleaseTestRelease;
        abstarctReleaseTestRelease= scenario.CreateOrderByClient(prod1,"1",id_order,typeofreceipt,selfdischargepoint,placeofdekivery,cashlesspayment,comment);
        assertNotNull(abstarctReleaseTestRelease);
        assertEquals(Status_order.approved,abstarctReleaseTestRelease.getOrders().get(0).getStatus());
        id_order="2";
        typeofreceipt="gcgxrtgth";
        selfdischargepoint="Moscow Mira 15";
        placeofdekivery = null;
        comment = "coment";
        cashlesspayment=true;
        abstarctReleaseTestRelease= scenario.CreateOrderByClient(prod2,"1",id_order,typeofreceipt,selfdischargepoint,placeofdekivery,cashlesspayment,comment);
        assertNotNull(abstarctReleaseTestRelease);
        assertEquals(Status_order.submitted_of_approval,abstarctReleaseTestRelease.getOrders().get(1).getStatus());
        Product product2 = new Product( "car" , "44444" , null,600.00,"cars",6);
        ProductfromClient productfromClient = new ProductfromClient(product2.getIdproduct(),2);
        prod2.add(productfromClient);
        id_order="3";
        abstarctReleaseTestRelease = scenario.CreateOrderByClient(prod2,"1",id_order,typeofreceipt,selfdischargepoint,placeofdekivery,cashlesspayment,comment);
        assertNotNull(abstarctReleaseTestRelease);
        assertEquals(Status_order.canceled,abstarctReleaseTestRelease.getOrders().get(2).getStatus());
    }

}