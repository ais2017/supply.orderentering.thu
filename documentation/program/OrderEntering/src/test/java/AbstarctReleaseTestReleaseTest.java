import example.AbstarctReleaseTestRelease;
import example.Client;
import example.Manager;
import example.Order;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbstarctReleaseTestReleaseTest {

    AbstarctReleaseTestRelease abstarctReleaseTestRelease;
    @Before
    public void CreateAbstarctReleaseTestRelease() {
        abstarctReleaseTestRelease = new AbstarctReleaseTestRelease();
    }

    @Test
    public void CheckCreate() throws Exception {
        assertNotNull(abstarctReleaseTestRelease);
        assertNotNull(abstarctReleaseTestRelease.getOrders());
        assertNotNull(abstarctReleaseTestRelease.getProducts());
        assertEquals(0,abstarctReleaseTestRelease.getOrders().size());
        assertEquals(0,abstarctReleaseTestRelease.getProducts().size());
    }

    @Test
    public void createProductList() throws Exception {
        abstarctReleaseTestRelease.CreateProductList();
        assertEquals(20,abstarctReleaseTestRelease.getProducts().size());
    }

    @Test
    public void showProductList() throws Exception {
        abstarctReleaseTestRelease.CreateProductList();
        String[] array={"14x63","14xx1","4x63","1460x","14x6","14x3","1463","1x63","146","x63","14x","xx8931",
                "xx893","xx89","xx89319","xx8978","xx893132","xx7862","xx1324","xx123456"};
        int i = 0;
        while (i< array.length) {
            assertEquals(array[i], abstarctReleaseTestRelease.getProducts().get(i).getIdproduct());
            i++;
        }
    }


    @Test
    public void forwardingOrder() throws Exception {
        Client client= new Client("Ivanov", "Ivan", "Ivanovich", "Pobeda 13", 999999, "mail@mail.ru");;
        Order order;
        order = new Order("32aa214", "liun", null,"Moscow",client,false,"this is comment");
        assertEquals(0,abstarctReleaseTestRelease.getOrders().size());
        assertNull(abstarctReleaseTestRelease.findOrder(order.getId_order()));
        abstarctReleaseTestRelease.ForwardingOrder(order);
        assertEquals(1,abstarctReleaseTestRelease.getOrders().size());
        assertNotNull(abstarctReleaseTestRelease.findOrder(order.getId_order()));
        abstarctReleaseTestRelease.ForwardingOrder(order);
        assertEquals(1,abstarctReleaseTestRelease.getOrders().size());
    }

    @Test
    public void showAllOrder() throws Exception {
        Client client = new Client("Ivanov", "Ivan", "Ivanovich", "Pobeda 13", 999999, "mail@mail.ru");
        Order order, order1;
        order = new Order("32aa214", "liun", null,"Moscow",client,false,"this is comment");
        order1 = new Order("32aa21444", "liunnn", "Moscvorechye",null,client,true,null);
        Manager manager = new Manager("Ivanov","Ivan","Ivanovich",78962);
        abstarctReleaseTestRelease.ForwardingOrder(order);
        abstarctReleaseTestRelease.ForwardingOrder(order1);
        assertNotNull(abstarctReleaseTestRelease.ShowAllOrder(manager));

    }




    @Test
    public void showYourOrder() throws Exception {
        Client client = new Client("Ivanov", "Ivan", "Ivanovich", "Pobeda 13", 999999, "mail@mail.ru");
        Client client1 = new Client("Ivanovv", "Ivann", "Ivanovichh", "Pobeda 13", 999999, "mail@mail.ru");
        Order order, order1, order2;
        order = new Order("32aa214", "liun", null,"Moscow",client,false,"this is comment");
        order1 = new Order("32aa21444", "liunnn", "Moscvorechye",null,client,true,null);
        order2 = new Order("32aa21444", "liunnn", "Moscvorechye",null,client1,true,null);
        Manager manager = new Manager("Ivanov","Ivan","Ivanovich",78962);
        abstarctReleaseTestRelease.ForwardingOrder(order);
        abstarctReleaseTestRelease.ForwardingOrder(order1);
        abstarctReleaseTestRelease.ForwardingOrder(order2);
        assertNotNull(abstarctReleaseTestRelease.ShowYourOrder(client));
        assertEquals(2,abstarctReleaseTestRelease.ShowAllOrder(manager).size());
    }




}