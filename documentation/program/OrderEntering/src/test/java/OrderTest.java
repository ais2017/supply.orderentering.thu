
import example.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OrderTest {

    Client client;
    Manager manager;
    Product product1,product2,product3;
    ProductinOrder pr1,pr2,pr3,pr4;
    Order order,order1;

    @Before
    public void createEntity() {
        client = new Client("Ivanov", "Ivan", "Ivanovich", "Pobeda 13", 999999, "mail@mail.ru");
        manager = new Manager("Ivanov","Ivan","Ivanovich",78962);
        product1 = new Product("car","25x69g78t","blue car with number 124xxx777",500.23,"technique",25);
        product2 = new Product("carss","qq25x69g78t","blow car with number 124xxx777",100.23,"technique",100);
        product3 = new Product("carxx","25kiux69g78t","green car with number 124xxx777",9000.23,"technique",2);
        order = new Order("32aa214", "liun", null,"Moscow",client,false,"this is comment");
        order1 = new Order("32aa21444", "liunnn", "Moscvorechye",null,client,true,null);
        System.out.println("Orders created");
    }
    @Test
    public void getId_order() throws Exception {
        String string ="32aa214";
        assertTrue(string.equals(order.getId_order()));
        string ="32aa21444";
        assertTrue(string.equals(order1.getId_order()));
    }

    @Test
    public void getTypeofreceipt() throws Exception {
        String string = "liun";
        assertTrue(string.equals(order.getTypeofreceipt()));
        string = "liunnn";
        assertTrue(string.equals(order1.getTypeofreceipt()));
    }

    @Test
    public void getSelfdischargepoint() throws Exception {
        String string = "Moscvorechye";
        assertNull(order.getSelfdischargepoint());
        assertNotNull(order1.getSelfdischargepoint());
        assertTrue(string.equals(order1.getSelfdischargepoint()));
    }

    @Test
    public void getPlaceofdekivery() throws Exception {
        String string = "Moscow";
        assertNull(order1.getPlaceofdekivery());
        assertNotNull(order.getPlaceofdekivery());
        assertTrue(string.equals(order.getPlaceofdekivery()));
    }

    @Test
    public void getClient() throws Exception {
        assertEquals(client,order.getClient());
        assertEquals(client,order1.getClient());
    }

    @Test
    public void getManager() throws Exception {
        assertNull(order.getManager());
        assertNull(order1.getManager());
        order.ApproveAnOrder(manager);
        assertNull(order.getManager());
    }

    @Test
    public void getCashlesspayment() throws Exception {
        assertFalse(order.getCashlesspayment());
        assertTrue(order1.getCashlesspayment());
    }

    @Test
    public void getComment() throws Exception {
        String string="this is comment";
        assertTrue(string.equals(order.getComment()));
        assertNull(order1.getComment());
    }

    @Test
    public void getStatus() throws Exception {
        assertEquals(Status_order.created,order.getStatus());
        assertEquals(Status_order.created,order1.getStatus());
        order.setStatus(Status_order.approved);
        assertEquals(Status_order.approved,order.getStatus());
    }

    @Test(expected = Exception.class)
    public void getStatus1() throws Exception {
        order.setStatus(Status_order.approved);
        order.setStatus(Status_order.canceled);
    }

    @Test(expected = Exception.class)
    public void getOrderCost() throws Exception {
        assertEquals(order.getOrderCost(),0.00,0.001);
        pr1 = new ProductinOrder(product1,100);

    }

    @Test
    public void AddProductinOrder() throws Exception{
        pr1 = new ProductinOrder(product1,10);
        pr2 = new ProductinOrder(product2,5);
        pr3 = new ProductinOrder(product3, 1);
        pr4 = new ProductinOrder(product1, 5);
        order.AddProductinOrder(pr1);
        order.AddProductinOrder(pr2);
        order.AddProductinOrder(pr3);
        Integer amount = 10;
        assertEquals(amount,product1.getAmount());
        assertEquals(amount,pr1.getProduct().getAmount());
        assertEquals(amount,order.findProductInOrder(pr1).getProduct().getAmount());
        Double cost = 14503.68;
        assertEquals(cost,order.getOrderCost(),0.001);
        amount = 10;
        assertEquals(amount, order.findProductInOrder(pr1).getAmount());
        order.AddProductinOrder(pr4);
        amount = 15;
        assertEquals(amount,order.findProductInOrder(pr1).getAmount());
        amount = 5;
        assertEquals(amount,order.findProductInOrder(pr2).getAmount());
        amount=1;
        assertEquals(amount,order.findProductInOrder(pr3).getAmount());
    }

    @Test
    public void getCostofProductinOrder() throws Exception {
        pr1 = new ProductinOrder(product1,10);
        order.AddProductinOrder(pr1);
        Double cost=order.getCostofProductinOrder(pr1);
        assertEquals(cost,5002.3,0.001);
        order.AddProductinOrder(pr1);
        cost = order.getCostofProductinOrder(pr1);
        assertEquals(cost,10004.6,0.001);
    }


    @Test
    public void deleteProductinOrder() throws Exception {
        pr1 = new ProductinOrder(product1,9);
        pr2 = new ProductinOrder(product2,77);
        pr3 = new ProductinOrder(product1,9);
        order.AddProductinOrder(pr1);
        order.AddProductinOrder(pr2);
        order.AddProductinOrder(pr3);
        System.out.println(order.getProducts().get(0).getAmount() + "        " + pr1.getProduct().getAmount() );
        System.out.println(order.getProducts().get(1).getAmount() + "        " + pr2.getProduct().getAmount() );
        order.DeleteProductinOrder(pr1.getProduct(),5);
        System.out.println(order.getProducts().get(0).getAmount() + "        " + pr1.getProduct().getAmount() );
        System.out.println(order.getProducts().get(1).getAmount() + "        " + pr2.getProduct().getAmount() );
        Integer amount =13;
        assertEquals(amount,order.findProductInOrder(pr1).getAmount());
        amount = 12;
        assertEquals(amount,order.findProductInOrder(pr1).getProduct().getAmount());
    }


    @Test
    public void approveAnOrder1() throws Exception {
        order.ApproveAnOrder(manager);
        assertEquals(Status_order.approved,order.getStatus());
    }

}