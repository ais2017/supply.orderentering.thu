import example.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProductTest {

    Product product;
    @Before
    public void createProduct(){
        product= new Product("car","25x69g78t","blue car with number 124xxx777",500.23,"technique",25);
        System.out.println("example.Product created");
    }

    @Test
    public void getName() throws Exception {
        String expected = "car";
        assertTrue(expected.equals(product.getName()));
    }

    @Test
    public void getIdproduct() throws Exception {
        String expected="25x69g78t";
        assertTrue(expected.equals(product.getIdproduct()));
    }

    @Test
    public void getDescription() throws Exception {
        String expected = "blue car with number 124xxx777";
        assertTrue(expected.equals(product.getDescription()));
    }

    @Test
    public void getPrice() throws Exception {
        Double price=500.23;
        assertEquals(price,product.getPrice(),0.001);
    }

    @Test
    public void getCategory() throws Exception {
        String expected = "technique";
        assertTrue(expected.equals(product.getCategory()));
    }

    @Test
    public void getAmount() throws Exception {
        Integer count = 25;
        assertEquals(count,product.getAmount());
    }

    @Test
    public void setAmount() {
        Integer count = 15;
        product.setAmount(15);
        assertEquals(count,product.getAmount());
    }

}