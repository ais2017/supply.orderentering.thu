import example.Product;
import example.ProductinOrder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProductinOrderTest {

    Product product;
    ProductinOrder productinOrder;
    @Before
    public void createProduct() throws Exception {
        product= new Product("car","25x69g78t","blue car with number 124xxx777",500.23,"technique",25);
        productinOrder = new ProductinOrder(product, 5);
        System.out.println("example.Product in example.Order created");
    }
    @Test
    public void getProduct() throws Exception {
        assertEquals(product,productinOrder.getProduct());
    }

    @Test
    public void getAmount() throws Exception {
        Integer count = 5;
        assertEquals(count,productinOrder.getAmount());
    }

    @Test
    public void getPrice() throws Exception {
        Double price = 2501.15;
        assertEquals(price, productinOrder.getPrice(),0.001);
    }


    @Test
    public void addproduct() throws Exception {
        productinOrder.Addproduct(5);
        Integer count = 10;
        assertEquals(count,productinOrder.getAmount());
        Double price = 5002.3;
        assertEquals(price,productinOrder.getPrice(),0.001);
    }



}