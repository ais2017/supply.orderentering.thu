
import example.Manager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ManagerTest {

    Manager manager;
    @Before
    public void createManager(){
        manager = new Manager("Ivanov","Ivan","Ivanovich",78962);
        System.out.println("example.Manager created");
    }

    @Test
    public void getName() throws Exception {
        String expected = "Ivan";
        assertTrue(expected.equals(manager.getName()));
    }

    @Test
    public void getMiddle_name() throws Exception {
        String expected = "Ivanovich";
        assertTrue(expected.equals(manager.getMiddle_name()));
    }

    @Test
    public void getSurname() throws Exception {
        String expected = "Ivanov";
        assertTrue(expected.equals(manager.getSurname()));
    }

    @Test
    public void getPhone() throws Exception {
        Integer expected = 78962;
        assertEquals(expected,manager.getPhone());
        expected = 12345;
        assertNotEquals(expected,manager.getPhone());
    }

}