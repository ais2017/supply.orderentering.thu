import static org.junit.Assert.*;

import example.Client;
import org.junit.Before;
import org.junit.Test;


public class ClientTest {

    Client client;
    @Before
    public void createClient() {
       client = new Client("Ivanov", "Ivan", "Ivanovich", "Pobeda 13", 999999, "mail@mail.ru");
        System.out.println("example.Client created");
    }
    @Test
    public void getSurname() throws Exception {
        String expected = "Ivanov";
        assertTrue(expected.equals(client.getSurname()));
    }

    @org.junit.Test
    public void getName() throws Exception {
        String expected = "Ivan";
        assertTrue(expected.equals(client.getName()));
    }

    @org.junit.Test
    public void getMiddle_name() throws Exception {
        String expected = "Ivanovich";
        assertTrue(expected.equals(client.getMiddle_name()));
    }

    @org.junit.Test
    public void getAddress() throws Exception {
        String expected = "Pobeda 13";
        assertTrue(expected.equals(client.getAddress()));
    }

    @org.junit.Test
    public void getPhone() throws Exception {
        Integer expected = 999999;
        assertEquals(expected,client.getPhone());
    }

    @org.junit.Test
    public void getMail() throws Exception {
        String expected = "mail@mail.ru";
        assertTrue(expected.equals(client.getMail()));
    }

}